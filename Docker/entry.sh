#!/bin/sh
# Change Permissions of file
chmod -vR 755 /data/*
chown -vR suser: /data/
# Start Script
/bin/su -s /bin/sh -c "sh /opt/AAC-to-MP3-stream.sh /data/url.txt > /tmp/vlc-log.log 2>&1" suser
# start cron
cat /tmp/*
/usr/sbin/crond -f -l 8

