#!/bin/bash
pkill -e vlc
pkill -e cvlc
[ -z "$1" ] && echo "No argument supplied. exiting" && exit 1
FILE=$1
if ! [[ -r $FILE && -w $FILE ]]; then
    echo "Unable to read $1 - exiting"
    exit 1
fi
baseport=30000
echo "using file $FILE"
while IFS= read -r line
do
    [[ -z "$line" ]] && echo "skipping empty line..." && break
    streamport=$(($baseport + 10))
    logfile="/tmp/vlclog-stream$streamport.log"
    date > $logfile
    echo "starting MP3 stream on port $streamport for $line" | tee -a $logfile
    cvlc $line --sout "#transcode{acodec=mp3,ab=192,channels=2,samplerate=48000,scodec=none}:http{mux=mp3,dst=:$streamport/go.mp3}" :no-sout-all:sout-keep >> $logfile 2>&1 &
    baseport=$streamport
    echo "VLC Launched."
    while : ; do
        [[ -f "$logfile" ]] && break
        echo "Pausing until log file exists."
        sleep 1
    done
#    tail -n0 -f "$logfile" | sed '/mpeg4audio demux packetizer/ q' not working on alpine
    echo "waiting for fully initialization..." && \
    ( tail -f -n0 "$logfile"  & ) | grep -q -i "demux packetizer"
    echo "started stream on port $streamport"
    sleep 1
done < "$FILE"
echo "initialization completed."